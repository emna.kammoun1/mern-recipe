import mongoose, { mongo } from 'mongoose'

const RecipeShema = new mongoose.Schema({
    name:  {type : String, required:true},
    ingredients: [{type : String ,require : true}],
    instructions:  {type : String, required:true},
    imageUrl:  {type : String, required:true},
    cookingTime:  {type : Number, required:true},
    userOwner:  {type : mongoose.Schema.Types.ObjectId,ref : "users", required:true},




})
export  const RecipeModel = mongoose.model("recipes", RecipeShema)
